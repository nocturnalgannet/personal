<!--

Spike

Name: Array Copy

Purpose: To test the copying of an array and determine the effect of changing
a single value in the copied array.

Author: Philip Johnson
Date Created: 16 May 2017
Date Last Modified: 23 May 2017
Version 1.1

Built for PHP v5.2.6

-->

<html>

	<head>
		<title>Spike - Array Copy</title>
	</head>
	
	<body>
	
		<div id="spikeInfo">
	
			<h1>Spike - Array Copy</h1>
			
			<h2>Purpose</h2>
			<p>
			To test the copying of an array and determine the effect of changing a single value in a copied array.
			</p>
			
			<h2>Implementation Details</h2>
			<p>
			An empty array will be created and populated with values before being displayed. After being displayed, the
			array will be copied and both the original version and the copied version will have a value changed (values
			belonging to different keys will be changed). Both the original array and the copied array will then
			be displayed again for comparison to the original array before the modifications.
			</p>
			
			<h2>Notes</h2>
			<ul>
				<li>
				Built for PHP v5.2.6.
				</li>
			</ul>
			
		</div>
		
		<div id="spikeImplementation">
		
			<h2>Result</h2>
	
			<?php
			
			// Create an empty array
			$first = array();
			
			// Give the array some values
			$first["a"] = "first";
			$first["b"] = "second";
			$first["c"] = "third";
			
			// Display the array
			echo "<h3>Before Modification</h3>";
			
			echo "<h4>Original Array:</h4>";
			echo "<p>";
			foreach ($first as $key => $value) {
				echo $key . " - " . $value . "<br>";
			}
			echo "</p>";
			
			// Copy the array
			$second = $first;
			
			// Modify a value in the original array and the copied array
			$first["a"] = "fourth";
			$second["b"] = "fifth";
			
			// Display the original array and the copied array
			echo "<h3>After Modification</h3>";
			
			echo "<h4>Original Array:</h4>";
			echo "<p>";
			foreach ($first as $key => $value) {
				echo $key . " - " . $value . "<br>";
			}
			echo "</p>";
			
			echo "<h4>Modified Array:</h4>";
			echo "<p>";				
			foreach ($second as $key => $value) {
				echo $key . " - " . $value . "<br>";
			}
			echo "</p>";
			
			?>
			
			<h2>Conclusion</h2>
			<p>
			Copying an array creates a completely new instance of that array. Modifying a value in either the
			original or copied version doesn't change the other.
			</p>
			
		</div>
		
	</body>
	
</html>
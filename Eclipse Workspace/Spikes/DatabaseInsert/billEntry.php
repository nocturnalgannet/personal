<?php
/**
 * Spike
 *
 * Name: Inserting into MySQL Database
 *
 * Purpose: To insert data into a MySQL Database using the mysqli library.
 *
 * Author: Philip Johnson
 * Date Created: 23 May 2017
 * Date Last Modified: 23 May 2017
 *
 * Built for PHP v5.2.6
 */

?>

<html>

	<head>
		<title>Spike - Inserting into MySQL Database</title>
	</head>
	
	<body>
	
		<div id="spikeInfo">
	
			<h1>Spike - Inserting into MySQL Database</h1>
			
			<h2>Purpose</h2>
			<p>
			To insert data into a MySQL Database using the mysqli library.
			</p>
			
			<h2>Implementation Details</h2>
			<p>
			Three forms will be displayed initially (and at the bottom of the page everytime it loads), one for
			Electricity Bills, one for Gas Bills and one for Water Bills. The user needs to enter data into one
			of the forms and then hit the submit button for that form. Each time the submit button is clicked,
			the following occurs. A connection to the CarbonMetrix Backup Database is established. If the
			connection fails, an error message is displayed. If the connection succeeds, the appropriate SQL
			INSERT statement is prepared. If that preparation fails, an error message is displayed and no more
			SQL processing occurs. If the preparation succeeds, variables are bound to the prepared statement
			for each piece of data that will be inserted. If the binding fails, an error message is displayed
			and no more SQL processing occurs. If the binding succeeds, each of the bound variables are given
			the appropriate value from the user input. The SQL INSERT statement is then executed. If the execution
			fails, an error message is displayed. The SQL INSERT statement resource is freed at this point as all
			SQL processing on the statement has been completed. If all SQL processing succeeded, a success message
			is displayed. The database connection is then closed and the user input cleared.
			</p>
			
			<h2>Notes</h2>
			<p>
			Built for PHP v5.2.6.
			</p>
			
		</div>
	
		<?php 
		
		// Process the bill if one has been uploaded
		if (isset($_POST["utilityType"])) {
			
			//Display results header
			echo("<h2>Bill Entering Result</h2>");
			
			// Require the connection.php file
			require("connection.php");
			
			// Connect to the database
			$db = new mysqli($server, $username, $password, $database);
			
			// Display an error message and exit if there was an error connecting to the database
			if ($db->connect_errno) {
				
				echo("<p>Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error . ".</p>");
				exit;
			
			}
			
			// Set success to true
			$success = TRUE;
			
			// Insert the bill using prepared statements
			switch ($_POST["utilityType"]) {
			
				case "Water":
					
					// Prepare the statement. Display an error message.
					if(!($stmt = $db->prepare("INSERT INTO tbl_cems_water_bill_backup(dt_start, dt_end, n_kl, n_total_cost, id_act, f_estimated)" .
					" VALUES (?, ?, ?, ?, ?, ?)"))) {
					
						echo("<p>(Water)Prepare failed: (" . $db->errno . ") " . $db->error . ".</p>");
						$success = FALSE;
					
					}
					
					// Bind the variables to the sql insert parameters. Display an error message.
					if($success && !($stmt->bind_param("ssidsi", $startDate, $endDate, $usage, $cost, $account, $estimated))) {
						
						echo("<p>(Water)Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error . ".</p>");
						$success = FALSE;
						
					}
					
					// Set the parameters values
					$startDate = $_POST["startDate"];
					$endDate = $_POST["endDate"];
					$usage = $_POST["usage"];
					$cost = $_POST["cost"];
					$account = $_POST["account"];
					if (isset($_POST["estimated"])) {
						$estimated = 1;
					} else {
						$estimated = 0;
					}
					
					// Execute the insert. Display an error message and exit if an error occurs.
					if ($success && !$stmt->execute()) {
						
						echo("<p>(Water)Execute failed: (" . $stmt->errno . ") " . $stmt->error . "</p>");
						$success = FALSE;
						
					}
					
					// Close the Statement
					if (isset($stmt)) {
						$stmt->close();
					}
					
					// Display success if no errors were encountered
					if ($success) {
						echo("<p>Water Bill added successfully.</p>");
					}
					
					// Break this case
					break;
						
				case "Elec":
					
					// Prepare the statement. Display an error message and exit if an error occurs.
					if(!($stmt = $db->prepare("INSERT INTO tbl_cems_elec_bill_backup(dt_start, dt_end, dt_month, dt_year, n_peak, n_offpeak, n_total_cost, id_act, f_estimated)" .
							" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"))) {
					
							echo("<p>(Elec)Prepare failed: (" . $db->errno . ") " . $db->error . ".</p>");
							$success = FALSE;
					
					}
					
					// Bind the variables to the sql insert parameters. Display an error message and exit if an error occurs.
					if($success && !($stmt->bind_param("ssiidddsi", $startDate, $endDate, $month, $year, $peakUsage, $offPeakUsage, $cost, $account, $estimated))) {
							
						echo("<p>(Elec)Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error . ".</p>");
						$success = FALSE;
							
					}
					
					// Set the parameters values
					$startDate = $_POST["startDate"];
					$endDate = $_POST["endDate"];
					$endDateComponents = explode("-", $_POST["endDate"]);
					$month = $endDateComponents[1];
					$year = $endDateComponents[2];
					$peakUsage = $_POST["peakUsage"];
					$offPeakUsage = $_POST["offPeakUsage"];
					$cost = $_POST["cost"];
					$account = $_POST["account"];
					if (isset($_POST["estimated"])) {
						$estimated = 1;
					} else {
						$estimated = 0;
					}
					
					// Execute the insert. Display an error message and exit if an error occurs.
					if ($success && !$stmt->execute()) {
							
						echo("<p>(Elec)Execute failed: (" . $stmt->errno . ") " . $stmt->error . "</p>");
						$success = FALSE;
							
					}
					
					// Close the Statement
					if (isset($stmt)) {
						$stmt->close();
					}
					
					// Display success
					if ($success) {
						echo("<p>Elec Bill added successfully.</p>");
					}
					
					// Break this case
					break;
						
				case "Gas":
					
					// Prepare the statement. Display an error message and exit if an error occurs.
					if(!($stmt = $db->prepare("INSERT INTO tbl_cems_ngas_bill_backup(dt_start, dt_end, n_mj, n_total_cost, id_act, f_estimated)" .
							" VALUES (?, ?, ?, ?, ?, ?)"))) {
					
							echo("<p>(Gas)Prepare failed: (" . $db->errno . ") " . $db->error . ".</p>");
							$success = FALSE;
					
					}
					
					// Bind the variables to the sql insert parameters. Display an error message and exit if an error occurs.
					if($success && !($stmt->bind_param("ssidsi", $startDate, $endDate, $usage, $cost, $account, $estimated))) {
							
						echo("<p>(Gas)Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error . ".</p>");
						$success = FALSE;
							
					}
					
					// Set the parameters values
					$startDate = $_POST["startDate"];
					$endDate = $_POST["endDate"];
					$usage = $_POST["usage"];
					$cost = $_POST["cost"];
					$account = $_POST["account"];
					if (isset($_POST["estimated"])) {
						$estimated = 1;
					} else {
						$estimated = 0;
					}
					
					// Execute the insert. Display an error message and exit if an error occurs.
					if ($success && !$stmt->execute()) {
							
						echo("<p>(Gas)Execute failed: (" . $stmt->errno . ") " . $stmt->error . "</p>");
						$success = FALSE;
							
					}
					
					// Close the Statement
					if (isset($stmt)) {
						$stmt->close();
					}
					
					// Display success
					if ($success) {
						echo("<p>Gas Bill added successfully.</p>");
					}
					
					// Break this case
					break;
			
			}
			
			// Close the database connection
			$db->close();
			
			// Unset the utilityType
			unset($_POST["utilityType"]);
			
		}
		
		?>
	
		<h2>Insert a Bill</h2>
	
		<!-- Form for inserting Water Bill -->
		<fieldset>
			<legend>Insert Water Bill</legend>
			<form method="post" action="billEntry.php">
				<input id="waterUtilityType" type="hidden" name="utilityType" value="Water">
				<table>
					<tr>
						<td>
							Start Date:
						</td>
						<td>
							<input id="waterStartDate" type="date" name="startDate" required>
						</td>
					</tr>
					<tr>
						<td>
							End Date:
						</td>
						<td>
							<input id="waterEndDate" type="date" name="endDate" required>
						</td>
					</tr>
					<tr>
						<td>
							Usage:
						</td>
						<td>
							<input id="waterUsage" type="text" name="usage" required>
						</td>						
					</tr>
					<tr>
						<td>
							Total Cost:
						</td>
						<td>
							<input id="waterCost" type="number" name="cost" required>
						</td>
					</tr>
					<tr>
						<td>
							Account:
						</td>
						<td>
							<input id="waterAccount" type="number" name="account" required>
						</td>
					</tr>
					<tr>
						<td>
							Estimated:
						</td>
						<td>
							<input id="waterEstimated" type="checkbox" name="estimated">
						</td>
					</tr>
					<tr>
						<td colspan=2 align="center">
							<input id="waterSubmit" type="submit" value="Enter Water Bill">
						</td>
					</tr>
				</table>
			</form>
		</fieldset>
		
		<!-- Form for inserting Elec Bill -->
		<fieldset>
			<legend>Insert Elec Bill</legend>
			<form method="post" action="billEntry.php">
			<input id="elecUtilityType" type="hidden" name="utilityType" value="Elec">
				<table>
					<tr>
						<td>
							Start Date:
						</td>
						<td>
							<input id="elecStartDate" type="date" name="startDate" required>
						</td>
					</tr>
					<tr>
						<td>
							End Date:
						</td>
						<td>
							<input id="elecEndDate" type="date" name="endDate" required>
						</td>
					</tr>
					<tr>
						<td>
							Peak Usage:
						</td>
						<td>
							<input id="elecPeakUsage" type="number" name="peakUsage" required>
						</td>
					</tr>
					<tr>
						<td>
							Off Peak Usage:
						</td>
						<td>
							<input id="elecOffPeakUsage" type="number" name="offPeakUsage">
						</td>
					</tr>
					<tr>
						<td>
							Total Cost:
						</td>
						<td>
							<input id="elecCost" type="number" name="cost" required>
						</td>
					</tr>
					<tr>
						<td>
							Account:
						</td>
						<td>
							<input id="elecAccount" type="number" name="account" required>
						</td>
					</tr>
					<tr>
						<td>
							Estimated:
						</td>
						<td>
							<input id="elecEstimated" type="checkbox" name="estimated">
						</td>
					</tr>
					<tr>
						<td colspan=2 align="center">
							<input id="elecSubmit" type="submit" value="Enter Elec Bill">
						</td>
					</tr>
				</table>
			</form>
		</fieldset>
		
		<!-- Form for inserting Gas Bill -->
		<fieldset>
			<legend>Insert Gas Bill</legend>
			<form method="post" action="billEntry.php">
				<input id="gasUtilityType" type="hidden" name="utilityType" value="Gas">
				<table>
					<tr>
						<td>
							Start Date:
						</td>
						<td>
							<input id="gasStartDate" type="date" name="startDate" required>
						</td>
					</tr>
					<tr>
						<td>
							End Date:
						</td>
						<td>
							<input id="gasEndDate" type="date" name="endDate" required>
						</td>
					</tr>
					<tr>
						<td>
							Usage:
						</td>
						<td>
							<input id="gasUsage" type="text" name="usage" required>
						</td>
					</tr>
					<tr>
						<td>
							Total Cost:
						</td>
						<td>
							<input id="gasCost" type="number" name="cost" required>
						</td>
					</tr>
					<tr>
						<td>
							Account:
						</td>
						<td>
							<input id="gasAccount" type="number" name="account" required>
						</td>
					</tr>
					<tr>
						<td>
							Estimated:
						</td>
						<td>
							<input id="gasEstimated" type="checkbox" name="estimated">
						</td>
					</tr>
					<tr>
						<td colspan=2 align="center">
							<input id="gasSubmit" type="submit" value="Enter Gas Bill">
						</td>
					</tr>
				</table>
			</form>
		</fieldset>
	
	</body>
	
</html>